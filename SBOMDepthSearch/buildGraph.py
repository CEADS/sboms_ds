import networkx as nx
import matplotlib.pyplot as plt

# The SBOM is being loaded in as a dictionary
# Many of the values are lists of dictionaries, including components and dependencies
# Dependencies are listed under 'bom-ref'

# TODO
# Iterate through list of dictionaries for components and get every component's name and bom-ref
# Then build a dictionary, with bom-ref as the key, name as the value
# Iterate through list of dictionaries for dependencies and build list of edges - bom-ref to bom-ref
# Then graph-like structure can be built using edges and component names

class SBOMGraph:
    def __init__(self):
        self.dic = None
        self.componentDict = None  # Initialized later
        self.edgeList = []
        self.graph = None

    def loadDic(self, dic):
        assert isinstance(dic, dict)
        self.dic = dic
        self.buildComponentDict()
        self.buildEdgeList()
        self.constructGraph()

    # Method that builds a dictionary that maps a component's bom-ref to its name
    # The keys are the bom-refs and the values are the component names
    def buildComponentDict(self):
        keys = []
        values = []
        # First add root component - it is not in the component list
        keys.append(self.dic['metadata']['component']['bom-ref'])
        values.append(self.dic['metadata']['component']['name'])

        # Add all components in the component list
        for component in self.dic['components']:
            keys.append(component['bom-ref'])
            values.append(component['name'])

        self.componentDict = dict(zip(keys, values))
        print("Component Dictionary:")
        print(self.componentDict)

    def getComponentDict(self):
        return self.componentDict

    # Method that builds a list of edges for the SBOM dependencies
    # Each edge is a tuple containing the bom-ref of the start component and the end component
    def buildEdgeList(self):
        for depend in self.dic['dependencies']:
            start = depend['ref']
            endList = depend['dependsOn']
            for end in endList:
                self.edgeList.append((start, end))
        print("Edge List:")
        print(self.edgeList)

    def getEdgeList(self):
        return self.edgeList

    # Method that constructs a graph structure from the component dictionary and edge list
    def constructGraph(self):
        self.graph = nx.Graph()
        for edge in self.edgeList:
            self.graph.add_edge(edge[0], edge[1])
        print(self.graph)

        try:
            nx.draw(self.graph)
            plt.savefig("graph.png")
            plt.close()
        except ModuleNotFoundError:  # The graph draw feature doesn't work on all SBOMs, so this is a temporary bandaid
            pass

    def search(self, component):
        # Get bom-ref for searched component
        ref = list(self.componentDict.keys())[list(self.componentDict.values()).index(component)]
        print("bom-ref for " + component + " is " + ref)

        # Get the path from the root to the searched component in bom-refs
        refPath = nx.shortest_path(self.graph, list(self.componentDict.keys())[0], ref)
        # Convert bom-ref path into component name path
        namePath = []
        for i in range(len(refPath)):
            namePath.append(self.componentDict[refPath[i]])
        print("Ref Path: " + str(refPath))
        print("Name Path: " + str(namePath))

        # Build a list of all components this component depends on
        depend = []
        for edge in self.edgeList:
            if edge[0] == ref:
                depend.append(self.componentDict[edge[1]])

        # Build a list of the vulnerabilities that affect this component
        #    This just parses out the information from the SBOM - it does not query any databases
        #    The code is contained in a try block, since it will result in a 'key error' if there
        #       is no vulnerability information present in the SBOM
        vulnerabilities = []
        try:
            # Get the vulnerability list from the SBOM dictionary
            vulnList = self.dic['vulnerabilities']
            for item in vulnList:
                if ref == item['affects'][0]['ref']:  # Check if any vulnerability references this component
                    # Add any that do and the first severity rating
                    vulnerabilities.append((item['id'], item['ratings'][0]['severity']))
        except KeyError:
            print("Key Error")
            pass

        print("Depends on: " + str(depend))
        print("Vulnerabilities: " + str(vulnerabilities))
        return (ref, depend, namePath, vulnerabilities)
