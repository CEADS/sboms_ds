# SBOMDepthSearch
A project that builds a graph structure out of an SBOM and allows users to query components within the SBOM.
Queries provide the following about the queried component:
- Component name
- bom-ref
- Dependencies on other components
- The path to get to the component from the root
- The numerical depth (the level of dependency-depth in the SBOM)
- Vulnerability information - **pulled from the loaded SBOM, not a database**

Users can also choose to save out all vulnerability information to a JSON file. These files can only be generated from SBOMs that have vulnerability information within them. These generated files contain the following information about all affected components:
- Component name
- Severity level of the first score source
  - Some SBOMs contain multiple sources, this only parses out the score from the first source
- The path to affected component from the root component
- The numerical depth (the level of dependency-depth in the SBOM)

## Status
All stated functionality in the project description is present and should be error free.

This project is currently in active development and additional functionality will be added as development continues.

There is some simple graphing visualization present in the project, which is in the very early stages of development.
It may or may not work with SBOMs that are loaded into the project. However, the project shouldn't crash when being used, even if you try to use this feature.

## User Manual
A user manual is not available at this time, but will be added in the future.

## Installation
This tool is not currently available as a .exe, a .tar, or similar. As such, it must be compiled and run locally. It was developed using the following:
- Python version 3.9
- PyQt version 5
- networkx version 3.1
- matplotlib version 3.7.1.

Please ensure you have all of these (or similar) installed locally before attempted use. **Note: PyQt version 5 is the only version that will work for this tool.**

#### Requirement Links
These links are provided purely for your convenience. There should be no expectation of any kind of warranty, liability, or endorsement for these sites.
- [Python](https://www.python.org/)
- [PyQt](https://wiki.python.org/moin/PyQt)
- [networkx](https://networkx.org/)
- [matplotlib](https://matplotlib.org/)

## License
The license for this tool can be found [here.](LICENSE)
