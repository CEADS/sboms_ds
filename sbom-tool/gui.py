import os.path
import pathlib
import signal
import sys
import json
from buildGraph import SBOMGraph

from whichpyqt import PYQT_VER
if PYQT_VER == 'PYQT5':
	from PyQt5.QtWidgets import *
	from PyQt5.QtGui import *
	from PyQt5.QtCore import *
else:
	raise Exception('Unsupported Version of PyQt: {}'.format(PYQT_VER))

class gui:
    def __init__(self):
        self.graph = SBOMGraph()
        self.window = QWidget()
        self.SetUp()
        self.Show()

    # Method that performs initial setup for the window
    def SetUp(self):
        self.window = QWidget()
        self.window.setWindowTitle("SBOM Depth Search")

        # Create load, displayFigure, parseVulnerabilities, search, and resetView buttons
        btnLoad = QPushButton("Load SBOM")
        btnLoad.clicked.connect(self.Load)
        btnDisplay = QPushButton("Display Figure")
        btnDisplay.clicked.connect(self.DisplayFigure)
        btnParseVulnerabilities = QPushButton("Parse Vulnerabilities from SBOM")
        btnParseVulnerabilities.clicked.connect(self.ParseVulnerabilities)
        btnSearch = QPushButton("Search")
        btnSearch.clicked.connect(self.Search)
        btnResetView = QPushButton("Reset View")
        btnResetView.clicked.connect(self.ShowComponents)

        # Create the form layout and add an editable text box
        formLayout = QFormLayout()
        formLayout.addRow("Name of the Component to Search For:", QLineEdit())

        # Create a scrollable area for displaying information
        scroll = QScrollArea()
        scroll.setWidgetResizable(True)
        innerFrame = QFrame()
        innerFrame.setLayout(QGridLayout())
        # innerFrame.setLayout(QVBoxLayout())
        scroll.setWidget(innerFrame)

        # Create a horizontal layout to hold display and parseVulnerabilities buttons
        buttonBar = QHBoxLayout()
        buttonBar.addWidget(btnDisplay)
        buttonBar.addWidget(btnParseVulnerabilities)

        # Create the layout and add the widgets
        layout = QVBoxLayout()
        layout.addWidget(btnLoad)
        layout.addLayout(buttonBar)
        layout.addLayout(formLayout)
        layout.addWidget(btnSearch)
        layout.addWidget(scroll)
        layout.addWidget(btnResetView)

        self.window.setLayout(layout)

    # Method that shows the window
    def Show(self):
        self.window.resize(2000, 1500)
        self.window.show()

    # Method that displays all components in the SBOM, by name and bom-ref
    def ShowComponents(self):
        # Check if SBOM has been loaded first
        if self.graph.getComponentDict() is None:
            return

        # Grab a reference to the scroll area for displaying components
        scroll = self.window.children()[len(self.window.children()) - 2]
        assert isinstance(scroll, QScrollArea)

        # Empty scroll area of all widgets before filling
        #    It will empty on the first loaded SBOM, but won't be if another is loaded
        for i in reversed(range(scroll.widget().layout().count())):
            scroll.widget().layout().itemAt(i).widget().setParent(None)

        componDict = self.graph.getComponentDict()
        scroll.widget().layout().addWidget(QLabel("\t\tComponents in SBOM"), 0, 0)
        keyNum = 1
        for key in componDict.keys():
            scroll.widget().layout().addWidget(QLabel("bom-ref: " + key), keyNum, 1)
            scroll.widget().layout().addWidget(QLabel("Name: " + str(componDict[key])), keyNum, 0)
            keyNum += 1

    # Method that displays a graph of the SBOM components
    #    Alpha status
    def DisplayFigure(self):
        # Check if display graph was clicked before a SBOM was loaded
        if self.graph.getComponentDict() is None:
            pop = QMessageBox()
            pop.setText("A valid CycloneDX SBOM must be loaded before a graph can be displayed")
            pop.exec()
            return  # Exit method

        if not os.path.exists("graph.png"):
            pop = QMessageBox()
            pop.setText("No graph has been generated.\nThis feature is under active development.")
            pop.exec()
            return
        # Grab a reference to the scroll area for displaying components
        scroll = self.window.children()[len(self.window.children()) - 2]
        assert isinstance(scroll, QScrollArea)

        # Empty scroll area of all widgets before filling
        #    It will empty on the first loaded SBOM, but won't be if another is loaded
        for i in reversed(range(scroll.widget().layout().count())):
            scroll.widget().layout().itemAt(i).widget().setParent(None)

        label = QLabel()
        pixmap = QPixmap('graph.png')
        label.setPixmap(pixmap)
        scroll.widget().layout().addWidget(label, 0, 1)

    # Method that parses out the vulnerability information from the loaded SBOM and saves it to a file
    def ParseVulnerabilities(self):
        # Check if parse vulnerabilities was clicked before a SBOM was loaded
        if self.graph.getComponentDict() is None:
            pop = QMessageBox()
            pop.setText("A valid CycloneDX SBOM must be loaded before vulnerabilities can be parsed out.")
            pop.exec()
            return  # Exit method

        vulnList = self.graph.dic['vulnerabilities']
        try:
            # Check for keys that this feature uses
            vulnList[0]['affects'][0]['ref']
            vulnList[0]['id']
            vulnList[0]['ratings'][0]['severity']
        except (IndexError, KeyError):
            pop = QMessageBox()
            pop.setText("The loaded SBOM does not contain any vulnerability information, " +
                        "so no information can be parsed out.")
            pop.exec()
            return  # Exit the method

        # Create a folder to hold generated files, if it doesn't already exist
        pathlib.Path("Vulnerabilities").mkdir(parents=True, exist_ok=True)

        # Open the target file in write mode
        rootName = list(self.graph.getComponentDict().values())[0]
        file = open("Vulnerabilities/" + rootName + "_vulnerabilities.json", 'w')

        components = []
        vulnerabilities = []

        for item in vulnList:
            componentName = self.graph.getComponentDict()[item['affects'][0]['ref']]
            searchResults = self.graph.search(componentName)
            idDict = {'id': item['id']}
            severityDict = {'severity': item['ratings'][0]['severity']}
            pathDict = {'path': searchResults[2]}
            depthDict = {'depth': (len(searchResults[2]) - 1)}
            components.append(componentName)
            vulnerabilities.append( (idDict, severityDict, pathDict, depthDict) )
        vulnDict = dict(zip(components, vulnerabilities))
        file.write(json.dumps(vulnDict, indent=4))

        file.close()

        pop = QMessageBox()
        pop.setText("Vulnerability information has been parsed out and saved to " +
                    "\"Vulnerabilities/" + rootName + "_vulnerabilities.json\", " +
                                      "within the current working directory.")
        pop.exec()

    # Click event that allows a user to search for and upload a CycloneDX SBOM in JSON format
    def Load(self):
        # Open a file dialog in the current working directory and filter for JSON files
        fileName = QFileDialog.getOpenFileName(self.window, 'Open file', os.path.abspath('') + "/SBOMs", "JSON Files (*.json)")

        # Check if a file was selected
        if fileName[0] == "":
            return  # If not, exit method without continuing

        # Open selected file in read mode, load it into a dictionary, then close the file
        file = open(fileName[0], 'r')
        dic = json.load(file)
        file.close()

        try:
            # Ensure that mandatory components are present and this is a CycloneDX SBOM
            assert dic['bomFormat'] == 'CycloneDX'
            keys = list(dic.keys())
            keys.index('bomFormat')
            keys.index('specVersion')
            keys.index('version')
            # Check for keys that this program uses
            keys.index('components')
            keys.index('dependencies')
        except (KeyError, AssertionError, ValueError):
            pop = QMessageBox()
            pop.setText("File was not a valid CycloneDX SBOM.\nPlease load a CloneDX SBOM in JSON format.")
            pop.exec()
            return

        # Graph drawings get layered on top of each other
        #    Delete file and remake it to avoid this
        if os.path.exists("graph.png"):
            os.remove("graph.png")
        f = open("graph.png", 'w')
        f.close()

        # Load the SBOM dictionary into the graph
        self.graph.loadDic(dic)

        # Display all components in SBOM
        self.ShowComponents()



        ####### Test Code #######

        # scroll = self.window.children()[len(self.window.children()) - 2]
        # keyNum = 0
        # for key in dic.keys():
        #     scroll.widget().layout().addWidget(QLabel("Key: " + key), keyNum, 0)
        #     scroll.widget().layout().addWidget(QLabel("Value: " + str(dic[key])), keyNum, 1)
        #     keyNum += 1

        # for depend in dic['dependencies']:
        #     scroll.widget().layout().addWidget(QLabel("\tNew Dependency"), keyNum, 0)
        #     # scroll.widget().layout().addWidget(QLabel(""))
        #     keyNum += 1
        #     for key in depend:
        #         scroll.widget().layout().addWidget(QLabel("Key: " + key), keyNum, 0)
        #         scroll.widget().layout().addWidget(QLabel("Value: " + str(depend[key])), keyNum, 1)
        #         keyNum += 1

        # for component in dic['components']:
        #     scroll.widget().layout().addWidget(QLabel("\tNew Component"), keyNum, 0)
        #     scroll.widget().layout().addWidget(QLabel("Name: " + str(component['name'])), keyNum, 1)
        #     keyNum += 1
        #     for key in component:
        #         scroll.widget().layout().addWidget(QLabel("Key: " + key), keyNum, 0)
        #         scroll.widget().layout().addWidget(QLabel("Value: " + str(component[key])), keyNum, 1)
        #         keyNum += 1

    # Click event that searches for the user-specified component and returns it's depth in the SBOM
    def Search(self):
        # Make it so that enter by default, triggers search

        # Get user input
        input = self.window.findChild(QLineEdit).text()

        # Check if search was clicked before a SBOM was loaded
        if self.graph.getComponentDict() is None:
            pop = QMessageBox()
            pop.setText("A valid CycloneDX SBOM must be loaded before a search can be performed.")
            pop.exec()
            return  # Exit method
        # Check if anything was entered
        if input == "":
            pop = QMessageBox()
            pop.setText("A component name must be entered before a search can be performed.")
            pop.exec()
            return  # Exit method

        # Check if component exists in SBOM
        try:
            list(self.graph.getComponentDict().values()).index(input)
        except ValueError:  # If error caught, inform user and reset view
            print("Component \'" + input + "\' is not in the SBOM")
            pop = QMessageBox()
            pop.setText("Component \"" + input + "\" is not in the SBOM")
            pop.exec()
            self.ShowComponents()
            return

        # Send input to graph and catch result
        output = self.graph.search(input)

        # Display output
        # Grab a reference to the scroll area for displaying components
        scroll = self.window.children()[len(self.window.children()) - 2]
        assert isinstance(scroll, QScrollArea)

        # Empty scroll area of all widgets before filling
        for i in reversed(range(scroll.widget().layout().count())):
            scroll.widget().layout().itemAt(i).widget().setParent(None)

        # Add a title for search results
        scroll.widget().layout().addWidget(QLabel("\t\tSearch result"), 0, 0)

        # Display component name and its bom-ref
        scroll.widget().layout().addWidget(QLabel("Component Name: " + input), 1, 0)
        scroll.widget().layout().addWidget(QLabel("bom-ref: " + output[0]), 1, 1)

        # Display all components this component depends on
        keyNum = 2
        for compon in output[1]:
            scroll.widget().layout().addWidget(QLabel("Depends On Component: "), keyNum, 0)
            scroll.widget().layout().addWidget(QLabel(compon), keyNum, 1)
            keyNum += 1

        # Display the path to the component from the root and the depth it resides
        scroll.widget().layout().addWidget(QLabel("Path to Component from Root: " + str(output[2])), keyNum, 0)
        scroll.widget().layout().addWidget(QLabel("Depth of Component: " + str(len(output[2]) - 1)), keyNum, 1)

        # Display all found vulnerabilities for this component
        keyNum += 1
        for vuln in output[3]:
            scroll.widget().layout().addWidget(QLabel("Vulnerability:\t" + vuln[0]), keyNum, 0)
            scroll.widget().layout().addWidget(QLabel("Severity:\t" + vuln[1]), keyNum, 1)
            keyNum += 1


if __name__ == '__main__':
    # Graph drawings get layered on top of each other
    #    Delete file and remake it to avoid this
    if os.path.exists("graph.png"):
        os.remove("graph.png")

    # This line allows CNTL-C in the terminal to kill the program
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    app = QApplication(sys.argv)

    w = gui()
    sys.exit(app.exec())
