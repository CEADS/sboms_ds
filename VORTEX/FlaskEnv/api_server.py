from msilib.schema import File
import re
from flask import Flask, request, send_file, jsonify, render_template
import networkx as nx
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot
from pyvis.network import Network
import tempfile
import os
import matplotlib.pyplot as plt
from datetime import datetime
import pandas as pd
from flask_cors import CORS



app = Flask(__name__)
CORS(app)  # Enable CORS for all routes
# Declare a global variable to store the filename
graph_filename = None
edgeList = []
graph = None
dummyvuln_dic = {"cryptography", "CVE-2023-23931"}

def loadDic(dic):
    global sbom_dicts
    assert isinstance(dic, dict)
    sbom_dicts = dic

def buildComponentDict():
    global componentDict
    keys = []
    values = []
    keys.append(sbom_dicts['metadata']['component']['bom-ref'])
    values.append(sbom_dicts['metadata']['component']['name'])
    for component in sbom_dicts['components']:
        keys.append(component['bom-ref'])
        values.append(component['name'])
    componentDict = dict(zip(keys, values))
    

def buildEdgeList():
    # The crash issue was due to the egdeList building upon itself, instead of restarting on every run
    #   < edgeList = [] > results in failure to load the graph, but < edgeList.clear() > resolves the issue
    edgeList.clear()
    for depend in sbom_dicts['dependencies']:
        start = depend['ref']
        endList = depend.get('dependsOn', [])
        for end in endList:
            edgeList.append((start, end))
    

def zoom_handler(event):
    scale = 1.1
    ax = plt.gca()
    if event.button == 'down':
        ax.set_xlim(ax.get_xlim()[0] * scale, ax.get_xlim()[1] * scale)
        ax.set_ylim(ax.get_ylim()[0] * scale, ax.get_ylim()[1] * scale)
    elif event.button == 'up':
        ax.set_xlim(ax.get_xlim()[0] / scale, ax.get_xlim()[1] / scale)
        ax.set_ylim(ax.get_ylim()[0] / scale, ax.get_ylim()[1] / scale)
    plt.draw()

# Route for serving the graph visualization HTML
@app.route('/graph_visualization')
def serve_graph_visualization():
    # Get the filename from the URL query parameters
    filename = request.args.get('filename')

    # Check if the filename is provided
    if not filename:
        return 'Filename not provided.', 400

    # Check if the HTML file exists
    if os.path.exists(filename):
        # Return the HTML file
        return send_file(filename)
    else:
        # Return an error message if the HTML file does not exist
        return 'Graph visualization HTML file not found.', 404


def graphfilterbynode(node):
    filtered_graph = nx.Graph()
    for edge in edgeList:
        start, end = edge
        # Convert the start and end to tuples if they're lists
        if isinstance(start, list):
            start = tuple(start)
        if isinstance(end, list):
            end = tuple(end)
        start = componentDict[start]
        end = componentDict[end]
        if start == node or end == node:
            filtered_graph.add_edge(start, end)

    # Convert the nodes back to lists for labeling
    labels = {node: list(node) if isinstance(node, tuple) else node for node in filtered_graph.nodes}
    node_colors = []

    # Draw the filtered graph with node names
    pos = nx.spring_layout(filtered_graph)  # Positions for all nodes
    for node in filtered_graph.nodes():
        if node in dummyvuln_dic:
            node_colors.append('red')  # Set color to red for nodes in 'dummyvuln_dic'
        else:
            node_colors.append('lightblue')  # Set color to light blue for other nodes

    # Draw the filtered graph with colored nodes and fitted text
    plt.gcf().canvas.mpl_connect('scroll_event', zoom_handler)
    nx.draw(filtered_graph, pos, with_labels=True, labels=labels, node_color=node_colors,
            node_size=1000, font_size=10, font_color='black')

    # Add a title to the plot
    plt.title(f'SBOM Graph for {node}')
    plt.tight_layout()  # Adjust the layout to maximize the figure size
    
    # Save the filtered graph as an image
    timestamp = datetime.now().strftime("%Y%m%d%H%M%S")
    filename = f"filtered_graph_{timestamp}.png"
    plt.savefig(filename)
    plt.close()

    # Convert the filtered graph to a pyvis Network object
    net = Network(notebook=False)
    for node in filtered_graph.nodes():
        if node in dummyvuln_dic:
            node_colors.append('red')
        else:
            node_colors.append('lightblue')
        node_size = 300  # Set the desired node size
        font_size = 600  # Set the desired font size

        net.add_node(node, label=labels[node], color=node_colors[-1], size=node_size)

    for edge in filtered_graph.edges():
        net.add_edge(*edge)

    net.barnes_hut()
    net.show_buttons(filter_=['nodes'])

    # Save the filtered graph as an HTML file
    timestamp = datetime.now().strftime("%Y%m%d%H%M%S")
    filename = f"filtered_graph_{timestamp}.html"
    net.write_html(filename)
    
    return filename

@app.route('/filtered_image', methods=['GET'])
def filtered_image():
    node_name = request.args.get('node_name')
    filtered_graph_html = graphfilterbynode(node_name)

    # Return the filtered graph HTML content
    with open(filtered_graph_html, 'r') as file:
        html_content = file.read()

    return html_content

    # node_name = request.args.get('node_name')
    # filtered_graph_image = graphfilterbynode(node_name)
    
    # # Return the filtered graph image
    # return send_file(filtered_graph_image)
# ...

def constructGraph():
    global graph
    graph = nx.Graph()
    for edge in edgeList:
        start, end = edge
        if isinstance(start, list):
            start = tuple(start)
        if isinstance(end, list):
            end = tuple(end)
        start = componentDict[start]
        end = componentDict[end]
        graph.add_edge(start, end)

    return graph

@app.route('/graph', methods=['POST'])
def process_graph():
    data = request.json
    # Test code -- check what was sent
    #f = open("debugNotes.txt", 'w+')
    #f.write("\n" + str(data))
    #f.close()
    # End test code
    # File contents are being sent correctly and the graph is loading well
    #   But it only works once. After the first run, the server becomes overloaded and crashes with an Error 500
    #   Need to clear the server between runs to ensure it doesn't become overloaded
    
    #json_data = request.json
    sbom_dict = dict(data)
    loadDic(sbom_dict)
    buildComponentDict()
    buildEdgeList()

    graph = constructGraph()

    # Convert the graph data to a JSON representation
    graph_data = nx.node_link_data(graph)

    # Set the Content-Type header to application/json
    response = jsonify(graph_data)
    response.headers["Content-Type"] = "application/json"
    return response, 200

@app.route("/api/component/<component_name>")
def get_component_details(component_name):
    # Find the component in the SBOM data
    components = sbom_dicts['components']

    # Search for the component with the matching name
    for component in components:
        if component["name"].lower() == component_name.lower():
            return jsonify(component)

    # Component not found
    return jsonify({"error": "Component not found"}), 404

@app.route("/api/component/<component_name>/vulnerabilities")
def get_component_vulnerabilities(component_name):
    # Find the component in the SBOM data
    components = sbom_dicts['components']

    # Search for the component by name
    for component in components:
        if component["name"].lower() == component_name.lower():
            vulnerabilities = component.get("vulnerabilities")
            if vulnerabilities:
                return jsonify(vulnerabilities)
            else:
                return jsonify({"message": "No vulnerabilities found for the component"})

    # Component not found
    return jsonify({"error": "Component not found"}), 404

@app.route('/api/getall_vulnerabilities', methods=['GET'])
def get_vulnerabilities():
    result = []
    for vulnerability in sbom_dicts['vulnerabilities']:
        ratings = vulnerability.get("ratings", [])
        severity = next((rating.get("severity", "") for rating in ratings), "Not Available")

        # Get the ref from the affects array (if available)
        affects_ref = next((affect.get("ref", "") for affect in vulnerability.get("affects", [])), "")

        # Find the matching component name based on the affects_ref
        component_name = ""
        for component in sbom_dicts['components']:
            if component.get("bom-ref") == affects_ref:
                component_name = component.get("name", "")
                break

        vulnerability_data = {
            "name": component_name,
            "id": vulnerability.get("id", ""),
            "severity": severity,
            "score": next((rating.get("score", "") for rating in ratings), "Not Available"),
            "description": vulnerability.get("description", ""),
        }
        result.append(vulnerability_data)

    return jsonify(result)


# @app.route('/api/getall_vulnerabilities', methods=['GET'])
# def get_vulnerabilities():
#     result = []
#     for vulnerability in sbom_dicts['vulnerabilities']:
#         ratings = vulnerability.get("ratings", [])
#         severity = next((rating.get("severity", "") for rating in ratings), "Not Available")
#         vulnerability_data = {
#             "ref": vulnerability.get("bom-ref", ""),
#             "id": vulnerability.get("id", ""),
#             "severity": severity,
#             "score": next((rating.get("score", "") for rating in ratings), "Not Available"),
#             "description": vulnerability.get("description", ""),
#         }
#         result.append(vulnerability_data)
#     return jsonify(result)

# New API endpoint for exporting vulnerabilities data to XLSX
@app.route('/api/export_vulnerabilities', methods=['GET'])
def export_vulnerabilities():
    result = []
    for vulnerability in sbom_dicts['vulnerabilities']:
        ratings = vulnerability.get("ratings", [])
        severity = next((rating.get("severity", "") for rating in ratings), "Not Available")
        vulnerability_data = {
            "ref": vulnerability.get("bom-ref", ""),
            "id": vulnerability.get("id", ""),
            "severity": severity,
            "score": next((rating.get("score", "") for rating in ratings), "Not Available"),
            "description": vulnerability.get("description", ""),
        }
        result.append(vulnerability_data)
    
    # Convert the data to a pandas DataFrame
    df = pd.DataFrame(result)
    
    # Save the DataFrame to an Excel file
    output_file = "vulnerabilities_data.xlsx"
    df.to_excel(output_file, index=False)
    
    # Return a response to indicate the export is successful
    return f"Vulnerabilities data exported to {output_file}"

@app.route('/api/get_vulnerability_statistics', methods=['GET'])
def get_vulnerability_statistics():

    vulnerabilities = sbom_dicts['vulnerabilities']
    severity_count = {"low": 0, "medium": 0, "high": 0, "critical": 0}
    
    for vulnerability in vulnerabilities:
        ratings = vulnerability.get("ratings", [])
        severity = next((rating.get("severity", "").lower() for rating in ratings), "unknown")
        severity_count[severity] += 1
    
    total_vulnerabilities = len(vulnerabilities)
    
    # Calculate the percentages
    severity_percentages = {severity: count/total_vulnerabilities * 100 for severity, count in severity_count.items()}
    
    return jsonify(severity_percentages)

@app.route('/vulnerabilitystatistics')
def vulnerability_statistics_page():
    return render_template('vulnerabilitystatistics.html')

@app.route('/api/vulnerable-components', methods=['GET'])
def get_vulnerable_components():
    # Initialize counters for vulnerable and total components
    vulnerable_count = 0
    total_components = sbom_dicts['components']

    # Set to store vulnerable components
    vulnerable_components = set()

    # Check for vulnerabilities in components
    for component in total_components:
        component_bom_ref = component.get("bom-ref", "")
        for vulnerability in sbom_dicts['vulnerabilities']:
            vulnerability_affects = vulnerability.get("affects", [])
            for affect in vulnerability_affects:
                vulnerability_ref = affect.get("ref", "")
                if component_bom_ref == vulnerability_ref:
                    vulnerable_count += 1
                    # vulnerable_components.add(component["name"])
                    break

    # Convert total_components to the actual count (not the list itself)
    total_components_count = len(total_components)

    # Calculate percentages
    if total_components_count == 0:
        vulnerable_percentage = 0
        non_vulnerable_percentage = 0
    else:
        vulnerable_percentage = (vulnerable_count / total_components_count) * 100
        non_vulnerable_percentage = 100 - vulnerable_percentage

    response = {
        # "vulnerable_components": list(vulnerable_components),
        "vulnerable_percentage": vulnerable_percentage,
        "non_vulnerable_percentage": non_vulnerable_percentage
    }

    return jsonify(response)

@app.route('/vulnerablepercentage')
def vulnerability_percentage_page():
    return render_template('vulnerablepercentage.html')


if __name__ == '__main__':
    app.run()
