# Tool Task List
This document outlines the required tasks to reach the project's goals and functionality. This is a reference document for the tool's developers.

## Tasks
The following tasks are organized in a manner that is a logical sequence of completion, but they can be worked on/completed in parallel.

### Task 1: Understand User Requirements
Gather and analyze user requirements for the SBOM visualization tool
- SBOM database

### Task 2: Design the User Interface (UI)
Identify the key features, functionalities, and visualizations that users expect from the tool.
- SBOM data json analysis (verify the different types of SBOM as we need to create a tool which can handle any SBOM of type JSON (there will be a template cyclone DX, Depth level of Json can be decided before parsing can not work for undefined depth )
- Extract the relevant data from the analysis results, such as software components, relationships, and other attributes.
- Prepare the data in a suitable format compatible with the selected visualization libraries or frameworks .
- Verify the structure and validity of the imported SBOM data. Perform checks to ensure that the JSON files adhere to the specified format and schema.
- Capture the SBOM data and transform into graph using relevant features/buttons and navigation elements
Consider use cases to understand the target needs and goals for better understanding

### Task 3: API Development and Integration
- Identify the required APIs for data retrieval, transformation, and processing from the SBOM database or data sources.
- Implement logic to transform the retrieved data into a suitable format for visualization, such as JSON or graph data structures.
- Afterwards integration of the APIs with the UI and graph traversal components.
- Comprehensive documentation for the APIs, including endpoint details, input/output formats
- Develop algorithms and logic for analyzing software dependencies captured in the SBOM data.
- Implement data structures and algorithms to represent the dependency graph efficiently using BFS/DFS

### Task 4: Graph Visualization
- Design graph traversal algorithms, such as breadth-first search (BFS) or depth-first search (DFS), to navigate and explore the dependency graph.
- Incorporate filtering and highlighting mechanisms to allow users to focus on specific parts of the SBOM visualization.
- Optimize the performance of the graph traversal and analysis logic to handle large-scale SBOM datasets efficiently.

## Future Tasks
These tasks are not required to achieve a minimum prototype status for the tool, but will enhance the user experience and add additional functionality.
- Use of database for storing graphs
- Utilize the libraries/frameworks to create interactive features like zooming, highlighting, and filtering.
- Ensure that the visualizations effectively convey insights and enable users to explore the SBOM data easily.
- Document any dependencies, installation instructions, or configuration settings required to run the SBOM visualization tool.
