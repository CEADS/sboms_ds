# VORTEX
A project that analyzes loaded SBOMs for vulnerabilities and displays a graphical representation of the findings.

## Project Goals
The project specific goals/tasks can be found [here.](ToolTaskList.md)

## Status
This project is currently in active development and is in the early stages of development. Working features will be listed below, as they are added.

## User Manual
A user manual is not available at this time, but will be added in the future.

## Installation
This tool is not currently available as a .exe, a .tar, or similar. As such, it must be compiled and run locally. It was developed using the following:
- Python version 3.9
- networkx version 3.1
- matplotlib version 3.7.1.

Please ensure you have all of these (or similar) installed locally before attempted use.

#### Requirement Links
These links are provided purely for your convenience. There should be no expectation of any kind of warranty, liability, or endorsement for these sites.
- [Python](https://www.python.org/)
- [networkx](https://networkx.org/)
- [matplotlib](https://matplotlib.org/)

## License
The license for this tool can be found [here.](LICENSE)
