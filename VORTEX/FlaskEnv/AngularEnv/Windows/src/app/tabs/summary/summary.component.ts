import { Component, OnInit } from '@angular/core';

interface DataItem {
  name: string;
  type: 'Application' | 'Library' | 'Package';
  depth: number;
}

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.css']
})



export class SummaryComponent implements OnInit {

  constructor() { }
  data: DataItem[] = [];
  componentNames: string[] = [
    "adduser",
    "aerospike-server-community",
    "apt",
    "base-files",
    "base-passwd",
    "bash",
    "bsdutils",
    "coreutils",
    "cryptography",
    "dash",
    "debconf"
    // Add more component names here if needed
  ];
  ngOnInit(): void {
    this.generateRandomData();
  }


  generateRandomData(): void {
    const types: ('Application' | 'Library' | 'Package')[] = ['Application', 'Library', 'Library', 'Library', 'Package'];

    for (let i = 0; i < this.componentNames.length; i++) {
      const componentName = this.componentNames[i];
      const randomTypeIndex = Math.floor(Math.random() * types.length);
      const randomDepth = Math.floor(Math.random() * 4); // Random depth between 0 and 3

      const newItem: DataItem = {
        name: componentName,
        type: types[randomTypeIndex],
        depth: randomDepth
      };

      this.data.push(newItem);
    }

  }
}
