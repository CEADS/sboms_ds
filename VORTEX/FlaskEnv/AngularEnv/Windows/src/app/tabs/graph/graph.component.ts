import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { FilePathService } from '../../service/file-path.service';
import { DataSet, Edge, Node, Data, Network } from 'vis-network/standalone';
import { select } from 'd3';

@Component({
  selector: 'app-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.css']
})
export class GraphComponent implements OnInit, OnDestroy {
  errorMessage: string | null = null; // To store any error message
  loadingProgress = 0; // To store the loading progress
  htmlContentLoaded = false; // Flag to track if HTML content is loaded
  htmlContent: string | undefined; // To store the fetched HTML content (add this line)
  selectedFilePath: string = '';
  private fetchHTMLSubscription: Subscription | undefined;
  private progressSubscription: Subscription | undefined;

  constructor(private http: HttpClient, private filePathService: FilePathService) { }

  ngOnInit(): void {
    this.selectedFilePath = this.filePathService.getFileContents(); // Attempting to pass the file contents
    this.fetchGraphData();
    // this.constructGraph();
  }

  ngOnDestroy(): void {
    this.fetchHTMLSubscription?.unsubscribe();
    this.progressSubscription?.unsubscribe();
  }

  async fetchGraphData(): Promise<void> {

    // const jsonData = {
    //   "nodes": [
    //     { "id": "aerospike" },
    //     { "id": "debian" },
    //     // Add more nodes as needed
    //   ],
    //   "links": [
    //     { "source": "aerospike", "target": "debian" },
    //     // Add more links as needed
    //   ]
    // };
    console.log("sent file", this.selectedFilePath); // Check what is going to be sent

    // Send hardcoded path - temp until solution is found
    //const jsonData1 = await this.http.get<any>('assets/aerospike-vdr.json').toPromise(); // Temp hardcoded path
    //const response = await this.http.post<any>('http://127.0.0.1:5000/graph', jsonData1).toPromise();

    // Working on sending actual user selected path
    //const jsonData = await this.http.get<any>(this.selectedFilePath).toPromise();
    const response = await this.http.post<any>('http://127.0.0.1:5000/graph', this.selectedFilePath).toPromise(); // This sends the file correctly
    console.log((response));
    // Create an empty array to hold the edges
    const edges: Edge[] = [];
    const edges1 = new DataSet<any>(response.links);

    // Convert the links array to edges array format
    edges1.forEach(link => {
      const edge: Edge = {
        from: link.source,
        to: link.target
      };
      // console.log("links", link.source, link.target)
      edges.push(edge);
    });
  
    // // Convert the nodes array to nodes array format
    // const nodes = new DataSet<Node>(response.nodes);
    const nodes = new DataSet<Node>(response.nodes.map((node:any) => ({
      id: node.id,
      label: node.id, // Set the label to the id
      size: 20 
    })));
  
    const data: Data = {
      nodes: nodes,
      edges: edges
    };
    const options= {
      nodes: {
        shape: 'circle',
        font: {
          face: 'Arial'
        },
        widthConstraint: {
          minimum: 50, // Set the minimum width of the node
          maximum: 50, // Set the maximum width of the node
        }
      },
      // You can customize other graph visualization options here
    }; 

    const container = document.getElementById('graphContainer');
  
    if (container) {
      const network = new Network(container, data, options);
      this.htmlContentLoaded = true;
    } else {
      this.errorMessage = 'Error: Graph container element not found.';
    }
  }

  }





// import { Component, OnInit } from '@angular/core';

// @Component({
//   selector: 'app-graph',
//   templateUrl: './graph.component.html',
//   styleUrls: ['./graph.component.css', '../shared_css.component.css']
// })
// export class GraphComponent implements OnInit {

//   constructor() { }

//   ngOnInit(): void {
//   }

// }
