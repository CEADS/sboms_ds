import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { TabsComponent } from "./tabs.component";
import { GraphComponent } from "./graph/graph.component";
import { SummaryComponent } from "./summary/summary.component";
import { StatsComponent } from "./stats/stats.component";
import { VulnerabilityComponent } from "./vulnerability/vulnerability.component";

const routes : Routes = [
    {
        path: '', component : TabsComponent, 
        children : [
            { path: '', redirectTo: 'graph', pathMatch: 'full' }, 
            {path: 'graph', component: GraphComponent},
            {path: 'summary', component :SummaryComponent},
            {path: 'stats', component: StatsComponent},
            {path: 'vulnerability', component: VulnerabilityComponent}
         ]

    }
]


@NgModule({
    imports : [RouterModule.forChild(routes)],
    exports : [RouterModule],

})


export class TabsRoutingModule{}