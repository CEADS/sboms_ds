import { Component, OnInit } from '@angular/core';
import { Color, ScaleType } from '@swimlane/ngx-charts';

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.css','../shared_css.component.css']
})
export class StatsComponent implements OnInit {

  ngOnInit(): void {
    
  }
  constructor() { }
  data1 = [
    { name: 'Category A', value: 100 },
    { name: 'Category B', value: 200 },
    { name: 'Category C', value: 150 },
    { name: 'Category D', value: 50 }
  ];

  data2 = [
    { name: 'Data Point 1', value: 300 },
    { name: 'Data Point 2', value: 150 },
    { name: 'Data Point 3', value: 250 },
    { name: 'Data Point 4', value: 100 }
  ];


  data3 = [
    {
      "name": "Germany",
      "value": 40632,
      "extra": {
        "code": "de"
      }
    },
    {
      "name": "United States",
      "value": 50000,
      "extra": {
        "code": "us"
      }
    },
    {
      "name": "France",
      "value": 36745,
      "extra": {
        "code": "fr"
      }
    },
    {
      "name": "United Kingdom",
      "value": 36240,
      "extra": {
        "code": "uk"
      }
    },
    {
      "name": "Spain",
      "value": 33000,
      "extra": {
        "code": "es"
      }
    },
    {
      "name": "Italy",
      "value": 35800,
      "extra": {
        "code": "it"
      }
    }
  ]

  data4 = [
     {
        "name": "Germany",
        "value": 36
      },

  ]

  colorScheme1: Color = {
    name: 'Variation',
    selectable: true,
    group: ScaleType.Ordinal,
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  colorScheme2: Color = {
    name: 'Variation2',
    selectable: true,
    group: ScaleType.Ordinal,
    domain: ['#1565c0', '#ef5350', '#9ccc65', '#ffa000']
  };




  
  legend: boolean = true;
  legendPosition: string = 'below';

  gradient = false;
  showLegend = true;

  showXAxis = true;
  showYAxis = true;
 
  showXAxisLabel = true;
  xAxisLabel = 'Country';
  showYAxisLabel = true;
  yAxisLabel = 'Population';

  onSelect(event: any) {
    console.log(event);
  }

}
