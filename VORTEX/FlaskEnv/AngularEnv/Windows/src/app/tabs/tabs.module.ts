import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { TabsComponent } from "./tabs.component";
import { GraphComponent } from "./graph/graph.component";
import { SummaryComponent } from "./summary/summary.component";
import { StatsComponent } from "./stats/stats.component";
import { VulnerabilityComponent } from "./vulnerability/vulnerability.component";
import { TabsRoutingModule } from "./tabs-routing.module";
import { NgxChartsModule } from "@swimlane/ngx-charts";


@NgModule({
    declarations:[
        TabsComponent,
        GraphComponent,
        SummaryComponent,
        StatsComponent,
        VulnerabilityComponent
    ],
    imports : [
        RouterModule,
        FormsModule, ReactiveFormsModule,
        CommonModule,
        TabsRoutingModule,
        NgxChartsModule
    ]
})

export class TabModule{}