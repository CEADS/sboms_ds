import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FilePathService } from '../service/file-path.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],
})
export class MainComponent implements OnInit {

  progress: number | null = null;
  showSpinner: boolean = false;
  isUploadComplete: boolean = false;
  selectedFilePath: string = '';

  constructor(private router: Router, private http: HttpClient, private filePathService: FilePathService) {}

  ngOnInit() {}

  onFileSelected(fileInput: HTMLInputElement) {
    const file = fileInput?.files?.[0];

    if (file) {
      const reader = new FileReader();

      reader.onload = (event) => {
        const content = event?.target?.result as string;
        try {
          const jsonData = JSON.parse(content);

          // If the parsing is successful, it's a valid JSON file
          this.selectedFilePath = file.name; // Store the file path
          //this.selectedFilePath = file.webkitRelativePath;

          //console.log("main - webkitRelativePath", file.webkitRelativePath); // This provides an empty string - browser won't allow us to know the path
          //console.log("main - file contents", jsonData); // This contains the whole SBOM - not best solution, but it will work (maybe?)
          this.filePathService.setFileContents(jsonData);
          this.filePathService.setFilePath(file.name); // Above doesn't store the file to the file service - only file name, though

          // Simulate file upload progress
          const totalSize = file.size;
          let uploadedSize = 0;

          const progressInterval = setInterval(() => {
            uploadedSize += 1024 * 1024; // Simulate uploading 1 MB at a time

            if (uploadedSize >= totalSize) {
              clearInterval(progressInterval);
              this.progress = 100;
              this.isUploadComplete = true;
            } else {
              this.progress = Math.round((100 * uploadedSize) / totalSize);
            }
          }, 100);

        } catch (error) {
          // Invalid JSON file, show error pop-up box or toast message
          alert('Invalid JSON file. Please select a valid JSON file.');
          fileInput.value = ''; // Clear the file input to allow reselection
        }
      };

      reader.readAsText(file);
    }
  }

  onButtonClicked() {
    // Hide the "Continue" button
    this.isUploadComplete = false;
    this.showSpinner = true;

    // Handle the button click event

    // Create a form with the selected file and POST to flask server
    // const formData = new FormData();
    // formData.append("SBOM", this.selectedFilePath);
    // const upload$ = this.http.post("http://localhost:4200/analysis/graph", formData);
    // upload$.subscribe();

    setTimeout(() => {
      // Navigate to the desired route after the task is complete
      this.router.navigate(['/analysis/']); // Replace 'other-route' with the route you want to navigate to

      // Reset the state to show the "Continue" button again
      this.showSpinner = false;
      this.isUploadComplete = true;
    }, 5000);
  }
}

// import { Component, OnInit } from '@angular/core';
// import { Router } from '@angular/router';

// @Component({
//   selector: 'app-main',
//   templateUrl: './main.component.html',
//   styleUrls: ['./main.component.css'],
// })
// export class MainComponent implements OnInit {

//   progress: number | null = null;
//   showSpinner: boolean = false;
//   isUploadComplete: boolean = false;
//   selectedFilePath: string = '';

//   constructor(private router: Router) {}

//   ngOnInit() {}

//   onFileSelected(fileInput: HTMLInputElement) {
//     const file = fileInput?.files?.[0];

//     if (file) {
//       const reader = new FileReader();

//       reader.onload = (event) => {
//         const content = event?.target?.result as string;
//         try {
//           const jsonData = JSON.parse(content);

//           // If the parsing is successful, it's a valid JSON file
//           this.selectedFilePath = file.name; // Store the file path

//           // Simulate file upload progress
//           const totalSize = file.size;
//           let uploadedSize = 0;

//           const progressInterval = setInterval(() => {
//             uploadedSize += 1024 * 1024; // Simulate uploading 1 MB at a time

//             if (uploadedSize >= totalSize) {
//               clearInterval(progressInterval);
//               this.progress = 100;
//               this.isUploadComplete = true;
//             } else {
//               this.progress = Math.round((100 * uploadedSize) / totalSize);
//             }
//           }, 100);

//         } catch (error) {
//           // Invalid JSON file, show error pop-up box or toast message
//           alert('Invalid JSON file. Please select a valid JSON file.');
//           fileInput.value = ''; // Clear the file input to allow reselection
//         }
//       };

//       reader.readAsText(file);
//     }
//   }

//   onButtonClicked() {
//     this.showSpinner = true;
//     // Handle the button click event

//     setTimeout(() => {
//       // Navigate to the desired route after the task is complete
//       this.router.navigate(['/analysis/']); // Replace 'other-route' with the route you want to navigate to
//     }, 5000);
//   }
// }

// import { Component, OnInit } from '@angular/core';
// import { Router } from '@angular/router';

// @Component({
//   selector: 'app-main',
//   templateUrl: './main.component.html',
//   styleUrls: ['./main.component.css'],
// })
// export class MainComponent implements OnInit {

//   progress: number | null = null;
//   showSpinner: boolean = false;
//   isUploadComplete: boolean = false;

//   constructor(private router: Router) {}

//   ngOnInit() {}

//   onFileSelected(fileInput: HTMLInputElement) {
//     const file = fileInput?.files?.[0];

//     if (file) {
//       const reader = new FileReader();

//       reader.onload = (event) => {
//         const content = event?.target?.result as string;
//         try {
//           const jsonData = JSON.parse(content);

//           // If the parsing is successful, it's a valid JSON file
//           // Simulate file upload progress
//           const totalSize = file.size;
//           let uploadedSize = 0;

//           const progressInterval = setInterval(() => {
//             uploadedSize += 1024 * 1024; // Simulate uploading 1 MB at a time

//             if (uploadedSize >= totalSize) {
//               clearInterval(progressInterval);
//               this.progress = 100;
//               this.isUploadComplete = true;
//             } else {
//               this.progress = Math.round((100 * uploadedSize) / totalSize);
//             }
//           }, 100);

//         } catch (error) {
//           // Invalid JSON file, show error pop-up box or toast message
//           alert('Invalid JSON file. Please select a valid JSON file.');
//           fileInput.value = ''; // Clear the file input to allow reselection
//         }
//       };

//       reader.readAsText(file);
//     }
//   }

//   onButtonClicked() {
//     this.showSpinner = true;
//     // Handle the button click event

//     setTimeout(() => {
//       // Navigate to the desired route after the task is complete
//       this.router.navigate(['/analysis/']); // Replace 'other-route' with the route you want to navigate to
//     }, 5000);
//   }
// }

// // upload.component.ts
// import { Component, OnInit } from '@angular/core';
// import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// import { HttpClient, HttpEventType, HttpHeaders, HttpRequest } from '@angular/common/http';
// import { Router } from '@angular/router';

// @Component({
//   selector: 'app-main',
//   templateUrl: './main.component.html',
//   styleUrls: ['./main.component.css'],
// })
// export class MainComponent implements OnInit {

//   progress: number | null = null;
//   showSpinner : boolean = false;
//   isUploadComplete: boolean = false;



//   constructor(private http: HttpClient,
//               private router: Router) {}

//   ngOnInit() {}



//   onFileSelected(fileInput: HTMLInputElement) {
//     const file = fileInput?.files?.[0];

//     if (file) {
//       // Simulate file upload progress
//       const totalSize = file.size;
//       let uploadedSize = 0;

//       const progressInterval = setInterval(() => {
//         uploadedSize += 1024 * 1024; // Simulate uploading 1 MB at a time

//         if (uploadedSize >= totalSize) {
//           clearInterval(progressInterval);
//           this.progress = 100;
//           this.isUploadComplete = true;
//         } else {
//           this.progress = Math.round((100 * uploadedSize) / totalSize);
//         }
//       }, 100);
//     }
//   }

//   onButtonClicked() {

//     this.showSpinner = true;
//     // Handle the button click event
   
//     setTimeout(() => {
//       // Navigate to the desired route after the task is complete
//       this.router.navigate(['/analysis/']); // Replace 'other-route' with the route you want to navigate to
//     }, 5000); 


  
//   }


//   // onFileSelected(fileInput: HTMLInputElement) {
//   //   const file = fileInput?.files?.[0];

//   //   if (file) {
//   //     console.log(file);
//   //     const formData = new FormData();
//   //     formData.append('file', file);

//   //     const uploadReq = new HttpRequest('POST', 'https://jsonplaceholder.typicode.com/todos/1', formData, {
//   //       reportProgress: true,
//   //     });

//   //     this.http.request(uploadReq).subscribe((event) => {
//   //       if (event.type === HttpEventType.UploadProgress) {
//   //         this.progress = Math.round((100 * event.loaded) / event.total!);
//   //       } else if (event.type === HttpEventType.Response) {
//   //         // File upload complete (optional: handle the response from the server)
//   //       }
//   //     });
//   //   }
//   // }




// }
