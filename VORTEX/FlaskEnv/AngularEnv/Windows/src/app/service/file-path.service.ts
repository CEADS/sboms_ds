import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FilePathService {
  private filePath = '';
  private fileContents = '';

  setFilePath(filePath: string) {
    this.filePath = filePath;
  }

  getFilePath() {
    return this.filePath;
  }

  // Added additional methods to try to send just the file contents
  setFileContents(fileContents: string) {
    this.fileContents = fileContents;
  }

  getFileContents() {
    return this.fileContents;
  }
}
