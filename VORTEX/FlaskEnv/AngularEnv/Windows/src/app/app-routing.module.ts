import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './main/main.component';
import { LandingComponent } from './landing/landing.component';

const routes: Routes = [
  {path: "landing", component: LandingComponent},
  {path: "main", component: MainComponent},
  {path: '', redirectTo: '/landing', pathMatch: 'full' },
  {path: 'analysis', loadChildren: () => import ("./tabs/tabs.module").then(tab => tab.TabModule)}

  
];



@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule { 
}
