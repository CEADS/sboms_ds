# Running VORTEX locally

## Prerequisites

Need to have the following installed on your machine
- python
- node.js
- angularCLI
- Flask
- networkx
- matplotlib
- pyvis
- pandas
- flask_cors

## Creating a Python Environment for Flask

### On Windows
In a terminal, navigate to ../FlaskEnv

	Run the following
	py -3 -m venv .venv

### On MacOS
In a terminal, navigate to ../FlaskEnv

	Run the following
	python3 -m venv .venv

## Starting the Front-End and Back-End Servers

### On Windows
In a terminal, navigate to ../FlaskEnv

	Run the following
	$ source .venv/Scripts/activate
	$ flask --app api_server run

**Note: On the first run, you will need to install all needed dependencies within the python environment.**

In another terminal, navigate to ../FlaskEnv/AngularEnv/Windows

	Run the following
	$ ng serve

**Note: On the first run, you will need to run 'npm install' before 'ng serve' to install the necessary modules.**

### On MacOS
In a terminal, navigate to ../FlaskEnv

	Run the following
	$ .venv/bin/activate
	$ flask --app api_server run

**Note: On the first run, you will need to install all needed dependencies within the python environment.**

In another terminal, navigate to ../FlaskEnv/AngularEnv/__MACOSX/WebTool/angular_latest

	Run the following
	$ ng serve

**Note: On the first run, you will need to run 'npm install' before 'ng serve' to install the necessary modules.**

## Locating the GUI

Open a web browser and navigate to http://localhost:4200
